/**
 * Created by pohchye on 18/7/2016.
 */

(function () {
    angular
        .module("queryApp", [])
        .controller("queryCtrl", queryCtrl);

    queryCtrl.$inject = ["$http"]; // to remove the white spaces, does not affect the logic
    function queryCtrl($http) {
        var vm = this;

        vm.employee = {};
        vm.employee.firstname = "";
        vm.employee.lastname = "";
        vm.employee.gender  = "";
        vm.employee.birthday = "";
        vm.employee.hiredate = "";
        vm.status = {
            message: "",
            code: 0
        };

        vm.ageCriteria = "";
        
        vm.empNo = 0;
        vm.result = null;
        vm.search = function () {
            $http.get("/api/employee/" + vm.empNo)
                .then(function (result) {
                    vm.result = result.data
                })
                .catch(function (error) {
                    vm.error = error;
                });
        };
        
        vm.searchByEmployeesByAge = function () {
            $http.post("/api/employee/save", vm.employee)
                .then(function (result) {
                    console.info("Result: " + result);
                    vm.status.message = "Inserted Successfully";
                    vm.status.code = 202;
                    vm.newEmployeeURL = result.data.abc;
                    console.info("URL: " + vm.newEmployeeURL);
                    // vm.result = result.data.url;
                })
                .catch(function () {
                    // console.info(error);
                    vm.status.message = "Error occurred";
                    vm.status.code = 400;
                    // vm.error = error.data;
                })
        };
        
    }
})();