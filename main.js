/**
 * Created by pohchye on 18/7/2016.
 */
// Create the Express app
var express=require("express");
var app=express();

// Path module
var path=require("path");

// Require this for POST method
var bodyParser=require("body-parser");
app.use(bodyParser.urlencoded({'extended': 'true'}));
app.use(bodyParser.json());

// Load mysql module
var mysql=require("mysql");
// Create mysql connection pool
var pool = mysql.createPool({
    host: "localhost",
    port: 3306,
    user: "pohchye",
    password: "pohchye",
    database: "employees",
    connectionLimit: 5
});

// Middleware
app.get("/api/employee/:emp_no", function (req, res) {
    pool.getConnection(function (err, connection) {
        if (err) {
            res.status(400).send(JSON.stringify(err));
            return;
        }
        connection.query("select * from employees where emp_no = ?",
            [req.params.emp_no],
            function (err, results) {
                connection.release();
                if (err) {
                    res.status(400).send(JSON.stringify(err));
                    return;
                }
                if (results.length)
                    res.json(results[0]);
                else
                    res.status(404).end("emp_no " + req.params.emp_no + " is not found");
            });
    });
});

// Age filter
const GET_EMPLOYEES_AGE = "select first_name, last_name, birth_date, year(curdate())- year(birth_date) as age from employees " +
    "where year(curdate())- year(birth_date)";
app.get("/api/employees/age/:age_criteria/:age", function (req, res) {
    console.info("Relation: " + req.params.age_criteria);
    console.info("Age : " + req.params.age);
    // Perform Queries
    pool.getConnection(function(err, connection) {
        if (err) {
            console.info("error connection");
            res.status(500).send(JSON.stringify(err));
            // return;
        } else {
            console.info("Good connection");
            // var pattern = "%" + req.params.pattern + "%";
            var sqlAgeCriteria = "";
            if (req.params.age_criteria == "gt") {
                sqlAgeCriteria = " > " + mysql.escape(req.params.age);
            } else if (req.params.age_criteria == "lt") {
                sqlAgeCriteria = " < " + mysql.escape(req.params.age);
            } else if (req.params.age_criteria == "eq") {
                sqlAgeCriteria = " = " + mysql.escape(req.params.age);
            }
            sqlAgeCriteria = sqlAgeCriteria + "limit 0, 10";
            connection.query(GET_EMPLOYEES_AGE + sqlAgeCriteria, [],
                function (err,results) {
                    connection.release();
                    if (err) {
                        console.info("Query Error");
                        res.status(500).send("Query Error: " + JSON.stringify(err));
                        // return;
                    } else {
                        for (var i in results) {
                            if (results.hasOwnProperty(i)) {
                                console.info(JSON.stringify(results[i]));
                                // console.info(results[i]);
                            }
                        }
                        res.json(results);
                    }
                });
        }
    });

});

// Serve public files
app.use(express.static(__dirname + "/public"));
// - For bower_components that is on the root directory instead of public
// app.use("/bower_components",express.static(path.join(__dirname, "bower_components")));
// app.use(express.static(path.join(__dirname,"public")));
// Handle Errors
app.use(function (req, res, http) {
    res.redirect("error.html");
});

// Set port
app.set("port",
    process.argv[2] || process.env.APP_PORT || 3000
);

// Start the Express server
app.listen(app.get("port"), function () {
    console.info("Express server started on port %s", app.get("port"));
});